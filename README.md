# cortexm
C++ application base for cortexM* microcontrollers.
The goal is to have hardware bits look like booleans.
The biggest improvement over what most IDE's generate is
a linker script that allows for ordering of static initializations across all modules.

This is all investigative work, use at  your own risk.
